import Vue from 'vue'
import VueRouter from 'vue-router'
import SystemMessages from './components/pages/messages/SystemMessages.vue'
import UserMessages from './components/pages/messages/UserMessages.vue'

Vue.use(VueRouter)
const lazyLoading = (path, ext = '.vue') => {
  return (resolve) => {
    require([`${path}${ext}`], resolve)
  }
}
const router = new VueRouter({
  routes: [
    {path: '/', component: lazyLoading('./components/pages/Sign')},
    {path: '/sign', component: lazyLoading('./components/pages/Sign')},
    {path: '/home', component: lazyLoading('./components/pages/Home')},
    {path: '/:userId/photos', component: lazyLoading('./components/pages/Myphoto')},
    {path: '/profile', component: lazyLoading('./components/pages/Profile'),
      children:[
        {
          path: 'changepassword',
          components: {
            profileView: lazyLoading('./components/pages/profiles/ChangePassword')
          }
        },
        {
          path: 'modifyinfo',
          components: {
            profileView: lazyLoading('./components/pages/profiles/ModifyInfo')
          }
        },
        {
          path: 'logout',
          components: {
            profileView: lazyLoading('./components/pages/profiles/Logout')
          }
        }
      ]
    },
    {path: '/upload', component: lazyLoading('./components/pages/Upload')},
    {path: '/search', component: lazyLoading('./components/pages/Search')},
    {path: '/about', component: lazyLoading('./components/pages/About')},
    {
      path: '/notifications',
      component: lazyLoading('./components/pages/Notifications'),
      children: [
        {
          path: 'system',
          components: {
            notificationView: SystemMessages
          }
        },
        {
          path: 'user',
          components: {
            notificationView: UserMessages
          }
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.path == '/sign') {
    next();
    return;
  }
  Vue.http.get("http://localhost:8080/loginStatus").then((response) => {
      if (response.body.result) {
        console.log(response.body)
        sessionStorage.setItem("userId", response.body.userId)
        next();
      } else {
        console.log(response.body)
        next("/sign");
      }
    }, (response) => {
      console.log(response.body)
      this.$message.error('Error!');
      next("/sign");
    }
  )
})


export default router
