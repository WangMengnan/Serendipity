import Vue from 'vue'
import App from './App'
import VueResource from 'vue-resource'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.css'
import MuseUI from 'muse-ui'
import 'muse-ui/dist/muse-ui.css'
import 'element-ui/lib/theme-default/index.css'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'


Vue.use(ElementUI, { locale })
Vue.use(MuseUI)
Vue.use(VueResource)
Vue.http.options.credentials = true;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  router: router
})
