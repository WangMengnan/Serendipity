/**
 * Created by Niko on 2017/3/8.
 */
import Vue from 'vue'
let api = {}
api.getCurrentUser = function () {
  return Vue.http.get("http://localhost:8080/users/" + sessionStorage.getItem("userId"))
}
export default api
