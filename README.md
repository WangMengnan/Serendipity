# Serendipity

A web-based interactive visualization and management system for photographies with social media features.

## Getting Started

Serendipity-server folder is the backend application
Serendipity-client folder is the frontend application
See deployment for notes on how to deploy the project on a live system.

## Server application

### Prerequisites

Tools required:

* Java
* Maven

### Running application

Run server application: Run SerendipityServerApplication.java

Runs on localhost by default


## Client application

### Prerequisites

Tools required:

* NodeJs

### Installing

Go to Serendipity-client folder
```
npm install
```

### Running application(Dev env)

Configure server address in 'config':
* dev.env.js;
* prod.env.js;
* sit.env.js

Run server application on development environment:
```
npm run dev
```
The project runs on localhost by default

### Deployment

Run
```
npm run build:prod
```
Then deploy the project in 'dist' folder in web server application

## Built With

* [Spring](https://spring.io/) - The server application framework used
* [NPM](https://www.npmjs.com/) - Frontend dependency management
* [Vue](https://vuejs.org/) - Frontend framework

## Author

* **Nick Wang**

