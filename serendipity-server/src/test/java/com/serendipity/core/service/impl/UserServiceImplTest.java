package com.serendipity.core.service.impl;

import com.serendipity.core.dao.UserDAO;
import com.serendipity.core.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by Niko on 2016/12/12.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {
    @Autowired
    private UserDAO userDAO;

    @Rollback
    @Test
    public void findUserByUsername() throws Exception {
        User user = new User();
        user.setUsername("Aaron");
        userDAO.save(user);
        User user2 = userDAO.findByUsername("Aaron");
        assertEquals(user.getUsername(), user2.getUsername());
        System.out.println(userDAO.findByUsername("Aaron").getId());
    }

    @Test
    public void addUser() throws Exception {

    }

}