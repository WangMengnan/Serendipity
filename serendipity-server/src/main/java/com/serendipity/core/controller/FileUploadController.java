package com.serendipity.core.controller;


import com.serendipity.core.pojo.Photo;
import com.serendipity.core.pojo.User;
import com.serendipity.core.service.PhotoService;
import com.serendipity.core.service.impl.PhotoServiceImpl;
import com.serendipity.core.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Niko on 4/19/2017.
 */


@RestController
public class FileUploadController {

    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Autowired
    private PhotoServiceImpl photoService;

    private String id;

    @Value("${web.upload-path}")
    private String uploadPath;

    @Autowired
    private UserServiceImpl userService;


    @RequestMapping(value = "/upload/id",method = RequestMethod.GET)
    public Map passUUID(HttpServletRequest request) {
        User user;
        id = UUID.randomUUID().toString();
        Map map = new HashMap<String, Object>();
        map.put("uuid",id);
        String userId =  (String)request.getSession().getAttribute("userId");
        System.out.println(userId);
        return map;
    }


    @RequestMapping("/upload")
    public Map fileUpload(@RequestParam("file") MultipartFile file,MultipartHttpServletRequest request) {

        String userId = (String)request.getSession().getAttribute("userId");
        System.out.println(userId+"upload");
        // file is not empty
        if (!file.isEmpty()) {
            try {
                // Saving path
                String filePath = uploadPath
                        + file.getOriginalFilename();
                // Transfer file
                file.transferTo(new File(filePath));
                //write into database
                photoService.uploadPhoto(id,filePath,userId,file.getOriginalFilename());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // return success
        Map map = new HashMap<String, Object>();
        map.put("result", true);

        return map;
    }

    @RequestMapping("upload/avatar")
    public Map uploadAvatar(@RequestParam("file") MultipartFile file, MultipartHttpServletRequest request){
        Map map = new HashMap<String, Object>();

        String userId = (String)request.getSession().getAttribute("userId");
        System.out.println(userId+"uploadAvatar");
        // file is not empty
        if (!file.isEmpty()) {
            try {
                // Saving path
                String filePath = uploadPath+"avatar/"
                        + file.getOriginalFilename();

                // Transfer file
                file.transferTo(new File(filePath));
                userService.updateAvatarById(userId,file.getOriginalFilename());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // return success\
        map.put("result", true);

        return map;
    }
}
