package com.serendipity.core.controller;

import com.serendipity.core.dao.PhotoDAO;
import com.serendipity.core.pojo.Photo;
import com.serendipity.core.pojo.User;
import com.serendipity.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by Niko on 2016/11/15.
 */

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private PhotoDAO photoDAO;

    @RequestMapping("/hello")
    public String getName() {
        return "Serendipity";
    }

    @RequestMapping(value = "/users",method = RequestMethod.GET)
    public List findUsers(@RequestParam String search) {
        List<User> userList;
        if(search != null)
            userList =  userService.findByUsernameLike(search);
        else
            userList = userService.findAll();
        return userList;
    }

    @RequestMapping(value = "/users/{userId}",method = RequestMethod.GET)
    public User getUserByUserId(@PathVariable String userId) {
        User user = userService.findUserById(userId);
        return user;
    }

    @RequestMapping(value = "/users/checkusername",method = RequestMethod.GET)
    public Map checkUsername(@RequestParam String username) {
        Map map = new HashMap<String, Object>();
        if (userService.findUserByUsername(username) != null) {
            map.put("result", false);
        } else map.put("result", true);
        return map;
    }

    @RequestMapping(value = "/users/checkemail",method = RequestMethod.GET)
    public Map checkEmail(@RequestParam String email) {
        Map map = new HashMap<String, Object>();
        if (userService.findUserByEmail(email) != null) {
            map.put("result", false);
        } else map.put("result", true);
        return map;
    }

    @RequestMapping(value = "/users/email",method =RequestMethod.POST)
    public Map changeEmail(@RequestBody User user) {
        Map map = new HashMap<String, Object>();
        userService.updateEmail(user);
        map.put("result",true);
        return map;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Map login(@RequestBody User user, HttpServletRequest request) {
        Map map = new HashMap<String, Object>();
        User aUser = userService.findUserByUsername(user.getUsername());
        if (aUser != null) {
            if (user.getPassword().equals(aUser.getPassword())) {
                request.getSession().setAttribute("userId", aUser.getId());
                String userId = aUser.getId();
                //System.out.println(userId+"login");
                //System.out.println(user.getUsername()+user.getEmail());
                map.put("userId", userId);
                map.put("result", true);
            } else {
                map.put("result", false);
            }
        } else {
            //no user exist
            map.put("result", false);
        }
        return map;
    }

    @RequestMapping(value = "/user/changepassword", method = RequestMethod.POST)
    public boolean changePassword(@RequestBody Map<String,Object> map,HttpServletRequest request) {
       String id = (String) map.get("id");
        String password = (String)map.get("password");
        String newPassword = (String)map.get("newPassword");

        System.out.println(id+password+newPassword);
       userService.changePassword(id,password,newPassword);
        request.getSession().removeAttribute("userId");
        return true;
    }


    @RequestMapping(value = "/logout",method = RequestMethod.GET)
    public void logout(HttpServletRequest request) {
        request.getSession().removeAttribute("user");
        System.out.println("log out");
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Map Register(@RequestBody User user, HttpServletRequest request) {
        Map map = new HashMap<String, Object>();
        if(userService.findUserByUsername(user.getUsername())!=null){
            map.put("result",false);
            return map;
        }
        userService.addUser(user);
        request.getSession().setAttribute("userId", userService.findUserByUsername(user.getUsername()).getId());
        //System.out.println(request.getSession().getAttribute("user")+ "register");

        map.put("userId", user.getId());
        map.put("result", true);
        return map;
    }

    @RequestMapping(value = "/loginStatus",method = RequestMethod.GET)
    public Map checkLoginStatus(HttpServletRequest request) {

        Map map = new HashMap<String, Object>();
        if (request.getSession().getAttribute("userId") == null) {
            System.out.println("empty session");
            map.put("result", false);
        } else {
           String userId = (String) request.getSession().getAttribute("userId");
            map.put("userId", userId);
            map.put("result", true);
            System.out.println("login as "+ userId);
        }
        return map;
    }

    @RequestMapping(value = "users/{userId}/photos",method = RequestMethod.GET)
    public List<Photo> getMyPhoto(@PathVariable String userId,HttpServletRequest request){
        System.out.println("get my photo");
        Map map = new HashMap<String,Object>();
        if(userId == null)
            return null;
        List<Photo> photoList;
        if(userId.equals(request.getSession().getAttribute("userId")))
            photoList = userService.getPhotosByUserId(userId,true);
        else
            photoList = userService.getPhotosByUserId(userId,false);
        return photoList;
    }

    @RequestMapping(value = "/users/follows",method = RequestMethod.POST)
    public void addFollow(@RequestBody Map map){
        String userId = (String) map.get("userId");
        String followedId = (String) map.get("followedId");
        userService.addFollow(userId,followedId);
    }

    @RequestMapping(value = "/users/follows",method = RequestMethod.DELETE)
    public void deleteFollow(@RequestParam String userId,@RequestParam String followedId){
        userService.deleteFollow(userId,followedId);
    }

    @RequestMapping(value = "/users/follows",method = RequestMethod.GET)
    public boolean checkFollow(@RequestParam String userId,@RequestParam String followedId){
        return userService.checkFollow(userId,followedId);
    }

    @RequestMapping(value = "/users/{userId}/follows",method = RequestMethod.GET)
    public List<User> getFollowing(@PathVariable String userId){
        return userService.getFollowByUserId(userId);
    }

    @RequestMapping(value = "/users/{userId}/follower",method = RequestMethod.GET)
    public List<User> getFollower(@PathVariable String userId){
        return userService.getFollowerByUserId(userId);
    }

}
