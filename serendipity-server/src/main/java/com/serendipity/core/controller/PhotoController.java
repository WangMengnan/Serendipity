package com.serendipity.core.controller;

import com.serendipity.core.dao.PhotoDAO;
import com.serendipity.core.pojo.Photo;
import com.serendipity.core.service.PhotoService;
import com.serendipity.core.service.impl.PhotoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Niko on 2017/3/8.
 */

@RestController
public class PhotoController {

    @Autowired
    private PhotoService photoService;

    @RequestMapping(value = "/photos", method = RequestMethod.GET)
    public List<Photo> getPhoto() {
        List<Photo> photoList = photoService.findAll();
        return photoList;
    }

    @RequestMapping(value = "/photos", method = RequestMethod.DELETE)
    public Map deletePhoto(@RequestParam String userId, @RequestParam String photoId) {
        Map map = new HashMap<String, Object>();
        try {
            if (photoService.deletePhotoById(userId, photoId)) {
                map.put("result", true);
            }else map.put("result",false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "/photo/info", method = RequestMethod.POST)
    public Map ModifyPhotoById(@RequestBody Photo photo) {

        Map map = new HashMap<String, Object>();
        System.out.println(photo);
        boolean result = photoService.updateById(photo);
        map.put("result", result);
        return map;
    }
}
