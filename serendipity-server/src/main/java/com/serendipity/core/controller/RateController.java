package com.serendipity.core.controller;

import com.serendipity.core.pojo.Rate;
import com.serendipity.core.service.RateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Niko on 5/7/2017.
 */
@RestController
public class RateController {
    @Autowired
    private RateService rateService;


    //takes userId, photoId, rate
    @RequestMapping(value = "/rates",method = RequestMethod.POST)
    public Map addRate(@RequestBody Rate rate){
        Map map = new HashMap<String, Object>();
        try {
            rateService.addRate(rate);
            map.put("result",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    //takes userId, photoId, rate
    @RequestMapping(value = "/rates",method = RequestMethod.DELETE)
    public Map deleteRate(@RequestBody Rate rate){
        Map map = new HashMap<String, Object>();
        try {
            rateService.deleteRate(rate);
            map.put("result",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    //takes photoId, return average rating
    @RequestMapping(value = "photos/{photoId}/rates",method = RequestMethod.GET)
    public double getPhotoRating(@PathVariable String photoId){
        List<Rate> rateList = rateService.findByPhotoId(photoId);
        double rating;
        int sum=0;
        for (int i=0;i<rateList.size();i++){
            sum = sum + rateList.get(i).getRate();
        }
        if(rateList.size() == 0)
            return 0;
        rating = sum*1.0/rateList.size();
        return rating;
    }

    //takes photoId, userId
    @RequestMapping(value = "/rates",method = RequestMethod.GET)
    public Rate getRate(@RequestParam String userId, @RequestParam String photoId){
        return rateService.findByUserIdAndPhotoId(userId,photoId);
    }


}
