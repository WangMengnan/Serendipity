package com.serendipity.core.controller;

import com.serendipity.core.dao.CommentDAO;
import com.serendipity.core.pojo.Comment;
import com.serendipity.core.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Niko on 2017/3/8.
 */

@RestController
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentDAO commentDAO;

//    @RequestMapping("/comments")
//    public List<Comment> getComments(){
//        return commentDAO.findAll();
//    }

    //takes userId, photoId,text
    @RequestMapping(value = "/comments",method = RequestMethod.POST)
    public Map addComment(@RequestBody Comment comment) {
        Map map = new HashMap<String, Object>();
        try {
            commentService.addComment(comment);
            map.put("result",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    //takes userId, photoId,text
    @RequestMapping(value = "/comments",method =RequestMethod.DELETE )
    public Map deleteComment(@RequestBody Comment comment) {
        Map map = new HashMap<String, Object>();
        try {
            commentService.delete(comment);
            map.put("result",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    //takes userId,photoId
    @RequestMapping(value = "/comments",method = RequestMethod.GET)
    public List<Comment> findByUseridAndPhotoId(@RequestParam String userId, @RequestParam String photoId){
        return commentService.findByUserIdAndPhotoId(userId,photoId);
    }

    //takes photoId
    @RequestMapping(value = "/photos/{photoId}/comments",method = RequestMethod.GET)
    public List<Comment> findByPhotoId(@PathVariable String photoId){
        return commentService.findByPhotoId(photoId);
    }

    //takes photoId
    @RequestMapping(value = "/users/{userId}/comments",method = RequestMethod.GET)
    public List<Comment> findByUserId(@PathVariable String userId){
        return commentService.findByUserId(userId);
    }

    //takes text
    @RequestMapping(value = "/comments/text",method = RequestMethod.GET)
    public List<Comment> findByTextLike(@RequestParam String text ){
        return commentService.findByTextLike(text);
    }
}
