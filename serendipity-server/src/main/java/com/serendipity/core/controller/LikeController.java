package com.serendipity.core.controller;

import com.serendipity.core.pojo.Like;
import com.serendipity.core.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Niko on 5/7/2017.
 */
@RestController
public class LikeController {

    @Autowired
    private LikeService likeService;


    //takes userId, photoId
    @RequestMapping(value = "/likes", method = RequestMethod.POST)
    public Map addLike(@RequestBody Like like) {
        Map map = new HashMap<String, Object>();
        try {
            likeService.addLike(like);
            map.put("result",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    //takes userId, photoId
    @RequestMapping(value = "/likes", method = RequestMethod.DELETE)
    public Map deleteLike(@RequestParam String userId, @RequestParam String photoId) {
        Map map = new HashMap<String, Object>();
        try {
            Like like = likeService.findByUserIdAndPhotoId(userId,photoId);
            likeService.deleteLike(like);
            map.put("result",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    //takes photoId
    @RequestMapping(value = "photos/{photoId}/likes",method = RequestMethod.GET)
    public List<Like> findByPhotoId(@PathVariable String photoId){
        return likeService.findByPhotoId(photoId);
    }

    //takes userId
    @RequestMapping(value = "/users/{userId}/likes",method = RequestMethod.GET)
    public List<Like> findByUserId(@PathVariable String userId){
        return likeService.findByUserId(userId);
    }

    //takes userId and photoId
    @RequestMapping(value = "/likes",method = RequestMethod.GET)
    public Like findByUserIdAndPhotoId(@RequestParam String userId, @RequestParam String photoId){
        return likeService.findByUserIdAndPhotoId(userId,photoId);
    }
}
