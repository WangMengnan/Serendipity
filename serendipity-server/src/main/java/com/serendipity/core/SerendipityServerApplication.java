package com.serendipity.core;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SerendipityServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SerendipityServerApplication.class, args);
	}

}
