package com.serendipity.core.dao;

import com.serendipity.core.pojo.Like;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Niko on 5/7/2017.
 */
public interface LikeDAO extends MongoRepository<Like,String> {
    public Like findLikeById(String id);
    public List<Like> findLikesByPhotoId(String photoId);
    public List<Like> findLikesByUserId(String userId);
    public Like findLikeByUserIdAndPhotoId(String userId,String photoId);

}
