package com.serendipity.core.dao;

import com.serendipity.core.pojo.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Niko on 2017/3/8.
 */
public interface CommentDAO extends MongoRepository<Comment,String>{
    public List<Comment> findCommentsByPhotoId(String photoId);
    public Comment findCommentById(String id);
    public List<Comment> findCommentsByUserId(String userId);
    public List<Comment> findCommentsByUserIdAndPhotoId(String userId,String photoId);
    public List<Comment> findCommentsByTextLike(String text);
}
