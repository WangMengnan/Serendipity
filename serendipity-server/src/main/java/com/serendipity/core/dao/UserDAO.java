package com.serendipity.core.dao;

import com.serendipity.core.pojo.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Niko on 2016/12/12.
 */
public interface UserDAO extends MongoRepository<User, String> {
    public User findByUsername(String username);
    public User findById(String id);
    public User findByEmail(String email);
    public void deleteByUsername(String username);
    public List<User> findByUsernameLike(String username);
}
