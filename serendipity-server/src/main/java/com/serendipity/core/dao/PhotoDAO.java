package com.serendipity.core.dao;

import com.serendipity.core.pojo.Photo;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Niko on 2017/3/8.
 */
public interface PhotoDAO extends MongoRepository<Photo, String> {
    public Photo findById(String id);
    public List<Photo> findPhotoByUserId(String userId);

}
