package com.serendipity.core.dao;

import com.serendipity.core.pojo.Rate;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Niko on 5/7/2017.
 */
public interface RateDAO extends MongoRepository<Rate,String> {
    public Rate findRateBy(String id);
    public Rate findRateByUserIdAndPhotoId(String userId,String photoId);
    public List<Rate> findRatesByPhotoId(String photoId);
    public List<Rate> findRatesByUserId(String userId);


}
