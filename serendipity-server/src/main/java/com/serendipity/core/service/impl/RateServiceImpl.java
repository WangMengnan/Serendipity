package com.serendipity.core.service.impl;

import com.serendipity.core.dao.RateDAO;
import com.serendipity.core.pojo.Rate;
import com.serendipity.core.service.RateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Niko on 5/7/2017.
 */
@Service
public class RateServiceImpl implements RateService {

    @Autowired
    private RateDAO rateDAO;

    @Override
    public void addRate(Rate rate) {
        Rate rate1 = rateDAO.findRateByUserIdAndPhotoId(rate.getUserId(),rate.getPhotoId());
        if(rate1==null){
            rate.setDate(new Date());
            rateDAO.save(rate);
        }else {System.out.println("rate existed");
            rate1.setRate(rate.getRate());
            rateDAO.save(rate1);
        }
    }

    @Override
    public void deleteRate(Rate rate) {
        rateDAO.delete(rate);
    }

    @Override
    public Rate findById(String id) {
        return rateDAO.findRateBy(id);
    }

    @Override
    public List<Rate> findByPhotoId(String photoId) {
        return rateDAO.findRatesByPhotoId(photoId);
    }

    @Override
    public List<Rate> findByUserId(String userId) {
        return rateDAO.findRatesByUserId(userId);
    }

    @Override
    public Rate findByUserIdAndPhotoId(String userId, String photoId) {
        return rateDAO.findRateByUserIdAndPhotoId(userId,photoId);
    }
}
