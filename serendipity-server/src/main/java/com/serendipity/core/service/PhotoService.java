package com.serendipity.core.service;

import com.serendipity.core.pojo.Photo;
import org.springframework.security.access.method.P;

import java.util.Date;
import java.util.List;

/**
 * Created by Niko on 2017/3/9.
 */
public interface PhotoService {
    public void addPhoto(Photo photo);
    public List<Photo> findPhotoByUserId(String userId);
    public Photo findPhotoById(String id);
    public List<Photo> findAll();
    public List<Photo> findByTag(String tag);
    public boolean updateById(Photo photo);
    public void uploadPhoto(String id, String url, String userId, String resourceName);
    public boolean deletePhotoById(String userId,String photoId);
}
