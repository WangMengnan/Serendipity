package com.serendipity.core.service.impl;

import com.serendipity.core.dao.LikeDAO;
import com.serendipity.core.pojo.Like;
import com.serendipity.core.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Niko on 5/7/2017.
 */
@Service
public class LikeServiceImpl implements LikeService {

    @Autowired
    private LikeDAO likeDAO;

    @Override
    public void addLike(Like like) {
        if (likeDAO.findLikeByUserIdAndPhotoId(like.getUserId(),like.getPhotoId())==null){
            like.setDate(new Date());
            likeDAO.save(like);
        }else System.out.println("like existed");
    }

    @Override
    public void deleteLike(Like like) {
            likeDAO.delete(like);
    }

    @Override
    public Like findById(String id) {
        return likeDAO.findLikeById(id);
    }

    @Override
    public List<Like> findByPhotoId(String photoId) {
        return likeDAO.findLikesByPhotoId(photoId);
    }

    @Override
    public List<Like> findByUserId(String userId) {
        return findByUserId(userId);
    }

    @Override
    public Like findByUserIdAndPhotoId(String userId, String photoId) {
        return likeDAO.findLikeByUserIdAndPhotoId(userId,photoId);
    }
}
