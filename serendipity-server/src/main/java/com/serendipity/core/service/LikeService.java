package com.serendipity.core.service;

import com.serendipity.core.pojo.Like;

import java.util.List;

/**
 * Created by Niko on 5/7/2017.
 */
public interface LikeService {
    public void addLike(Like like);
    public void deleteLike(Like like);
    public Like findById(String id);
    public List<Like> findByPhotoId(String photoId);
    public List<Like> findByUserId(String userId);
    public Like findByUserIdAndPhotoId(String userId,String photoId);
}
