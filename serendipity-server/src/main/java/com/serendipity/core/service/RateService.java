package com.serendipity.core.service;

import com.serendipity.core.pojo.Rate;

import java.util.List;

/**
 * Created by Niko on 5/7/2017.
 */
public interface RateService {
    public void addRate(Rate rate);
    public void deleteRate(Rate rate);
    public Rate findById(String id);
    public List<Rate> findByPhotoId(String photoId);
    public List<Rate> findByUserId(String userId);
    public Rate findByUserIdAndPhotoId(String userId,String photoId);
}
