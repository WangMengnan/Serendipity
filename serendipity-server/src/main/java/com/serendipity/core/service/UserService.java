package com.serendipity.core.service;

import com.serendipity.core.pojo.Photo;
import com.serendipity.core.pojo.User;

import java.util.List;

/**
 * Created by Niko on 2016/12/12.
 */
public interface UserService {

    public void addUser(User user);
    //public void addUser(String username,String password,String email);

    //public void deleteUser(User user);
    public void deleteUserByUsername(String username);


    public void changeEmail(String username,String newEmail);

    public List<User> findAll();
    public User findUserByUsername(String username);
    public User findUserById(String id);
    public User findUserByEmail(String email);
    public List<User> findByUsernameLike(String username);


    public List<String> getPhotosByUsername(String username);
    public List<Photo> getPhotosByUserId(String userId,boolean hasPrivate);

    public String getAvatarByUsername(String username);

    public List<User> getFollowByUserId(String userId);
    public boolean checkFollow(String userId,String followedId);
    public void addFollow(String userId,String followedId);
    public void deleteFollow(String userId,String followedId);
    public List<User> getFollowerByUserId(String userId);

    public void changePassword(String Id,String password,String newPassword);
    public void updateAvatar(User user,String avatar);
    public void updateAvatarById(String id,String avatar);
    public void updateEmail(User user);

    public void updateAvatarByUsername(String username,String avatar);
    public void addPhotoByUsername(String username,String photo);
    public void addFollowByUsername(String username,String follow);
    public void deletePhotoByUsername(String username,String photo);
    public void deleteFollowByUsername(String username,String follow);

    public int count() throws Exception;



}
