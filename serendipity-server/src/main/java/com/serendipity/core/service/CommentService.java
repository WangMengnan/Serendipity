package com.serendipity.core.service;

import com.serendipity.core.pojo.Comment;

import java.util.List;

/**
 * Created by Niko on 5/7/2017.
 */
public interface CommentService {
    public void addComment(Comment comment);
    public Comment findById(String id);
    public List<Comment> findByPhotoId(String photoId);
    public List<Comment> findByUserId(String userId);
    public void delete(Comment comment);
    public List<Comment> findByUserIdAndPhotoId(String userId,String photoId);
    public List<Comment> findByTextLike(String text);
}
