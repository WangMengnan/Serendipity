package com.serendipity.core.service.impl;

import com.serendipity.core.dao.CommentDAO;
import com.serendipity.core.dao.UserDAO;
import com.serendipity.core.pojo.Comment;
import com.serendipity.core.pojo.User;
import com.serendipity.core.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Niko on 5/7/2017.
 */
@Service
public class CommentServiceImpl implements CommentService{

    @Autowired
    private CommentDAO commentDAO;

    @Autowired
    private UserDAO userDAO;

    @Override
    public void addComment(Comment comment) {
        comment.setDate(new Date());
        User user = userDAO.findById(comment.getUserId());
        comment.setUsername(user.getUsername());
        commentDAO.save(comment);
    }

    @Override
    public Comment findById(String id) {
        return commentDAO.findCommentById(id);
    }

    @Override
    public List<Comment> findByPhotoId(String photoId) {
        return commentDAO.findCommentsByPhotoId(photoId);
    }

    @Override
    public List<Comment> findByUserId(String userId) {
        return commentDAO.findCommentsByUserId(userId);
    }

    @Override
    public void delete(Comment comment) {
        commentDAO.delete(comment);
    }

    @Override
    public List<Comment> findByUserIdAndPhotoId(String userId, String photoId) {
        return commentDAO.findCommentsByUserIdAndPhotoId(userId,photoId);
    }

    @Override
    public List<Comment> findByTextLike(String text) {
        return commentDAO.findCommentsByTextLike(text);
    }


}
