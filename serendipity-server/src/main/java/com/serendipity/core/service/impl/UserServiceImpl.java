package com.serendipity.core.service.impl;

import com.serendipity.core.dao.PhotoDAO;
import com.serendipity.core.dao.UserDAO;
import com.serendipity.core.pojo.Photo;
import com.serendipity.core.pojo.User;
import com.serendipity.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Niko on 2016/12/12.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private PhotoDAO photoDAO;


    @Override
    public User findUserByUsername(String username) {
        return userDAO.findByUsername(username);
    }

    @Override
    public User findUserById(String id) {
        return userDAO.findById(id);
    }

    @Override
    public User findUserByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    @Override
    public List<User> findByUsernameLike(String username) {
        List<User> userList = userDAO.findByUsernameLike(username);
        return userList;
    }

    @Override
    public List<String> getPhotosByUsername(String username) {
        return ((List<String>) userDAO.findByUsername(username).getPhotos());
    }

    @Override
    public List<Photo> getPhotosByUserId(String userId, boolean hasPrivate) {
        User user = userDAO.findById(userId);
        List<String> photos = user.getPhotos();
        List<Photo> photoList = new ArrayList<Photo>();
        for (int i = 0; i < photos.size(); i++) {
            Photo photo = photoDAO.findById(photos.get(i));
            if (photo.isIfPrivate() && !hasPrivate)
                continue;
            photoList.add(photo);
        }
        return photoList;
    }

    @Override
    public String getAvatarByUsername(String username) {
        return userDAO.findByUsername(username).getAvatar();
    }

    @Override
    public List<User> getFollowByUserId(String userId) {
        List<String> followList = userDAO.findById(userId).getFollow();
        List<User> userList = new ArrayList<>();
        if (followList == null) return null;
        for (int i = 0; i < followList.size(); i++) {
            userList.add(userDAO.findById(followList.get(i)));
        }
        return userList;

    }

    @Override
    public boolean checkFollow(String userId, String followedId) {
        List<String> followList = userDAO.findById(userId).getFollow();
        if (followList == null) return false;
        boolean flag = false;
        for (int i = 0; i < followList.size(); i++) {
            if (followList.get(i).equals(followedId)) {
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public void addFollow(String userId, String followedId) {
        User user = userDAO.findById(userId);
        List<String> followList = user.getFollow();
        if (followList == null) {
            followList.add(followedId);
            user.setFollow(followList);
        } else {
            boolean flag = false;
            for (int i = 0; i < followList.size(); i++) {
                if (followList.get(i) .equals( followedId)) {
                    flag = true;
                }
            }
            if (!flag) {
                followList.add(followedId);
                user.setFollow(followList);
            }
        }
        userDAO.save(user);
    }

    @Override
    public void deleteFollow(String userId, String followedId) {
        User user = userDAO.findById(userId);
        List<String> followList = user.getFollow();
        if (followList == null) {
            System.out.println("followed user not exist");
        }
        boolean flag= false;
        int j=0;
        for (int i = 0; i < followList.size(); i++) {
            if (followList.get(i).equals(followedId)) {
                j=i;
                flag =true;
            }
        }
        if (flag){
            followList.remove(j);
            user.setFollow(followList);
            userDAO.save(user);
        }else System.out.println("followed user not found");
    }

    @Override
    public List<User> getFollowerByUserId(String userId) {
        List<User> allUserList= userDAO.findAll();
        List<String> followList =new ArrayList<>();
        List<User> resultList= new ArrayList<>();
        for (int i=0;i<allUserList.size();i++){
            followList = allUserList.get(i).getFollow();
            for (int j=0;j<followList.size();j++){
                if (followList.get(j).equals(userId)){
                    resultList.add(allUserList.get(i));
                }
            }
        }
        return resultList;
    }


    @Override
    public void changePassword(String id, String password, String newPassword) {

        User user = userDAO.findById(id);
        if (user != null) {
            if (password.equals(user.getPassword())) {

                user.setPassword(newPassword);
                userDAO.save(user);
                System.out.println("password changed");
            }
        }
    }

    @Override
    public void updateAvatar(User user, String avatar) {
        User changedUser = userDAO.findById(user.getId());
        changedUser.setAvatar(avatar);
        try {
            userDAO.save(changedUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAvatarById(String id, String avatar) {
        User user = userDAO.findById(id);
        user.setAvatar(avatar);
        try {
            userDAO.save(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateEmail(User user) {
        User newUser = userDAO.findById(user.getId());
        newUser.setEmail(user.getEmail());
        userDAO.save(newUser);
    }

    @Override
    public void updateAvatarByUsername(String username, String avatar) {
        userDAO.findByUsername(username).setAvatar(avatar);
    }

    @Override
    public void addPhotoByUsername(String username, String photo) {
        if (!userDAO.findByUsername(username).getPhotos().contains(photo)) {
            userDAO.findByUsername(username).getPhotos().add(photo);
        } else System.out.println("Photo already exist");
    }

    @Override
    public void addFollowByUsername(String username, String follow) {
        if (!userDAO.findByUsername(username).getFollow().contains(follow)) {
            userDAO.findByUsername(username).getFollow().add(follow);
        } else System.out.println("Follow already exist");
    }

    @Override
    public void deletePhotoByUsername(String username, String photo) {
        List<String> photoList = userDAO.findByUsername(username).getPhotos();
        Boolean flag = false;
        for (int i = 0; i < photoList.size(); i++) {
            if (Objects.equals(photoList.get(i), photo)) {
                photoList.remove(i);
                System.out.println("Photo removed");
                flag = true;
            }
        }
        if (!flag) {
            System.out.println("Photo not found");
        }
    }

    @Override
    public void deleteFollowByUsername(String username, String follow) {
        List<String> followList = userDAO.findByUsername(username).getFollow();
        Boolean flag = false;
        for (int i = 0; i < followList.size(); i++) {
            if (Objects.equals(followList.get(i), follow)) {
                followList.remove(i);
                System.out.println("Follow removed");
                flag = true;
            }
        }
        if (!flag) {
            System.out.println("Follow not found");
        }
    }

    @Override
    public int count() throws Exception {
        long size = userDAO.count();
        int count = Integer.valueOf(String.valueOf(size));
        return count;
    }


    @Override
    public void addUser(User user) {
        if (userDAO.findByUsername(user.getUsername()) == null) {
            if (user.getPhotos() == null)
                user.setPhotos(new ArrayList<>());
            if (user.getFollow() == null)
                user.setFollow(new ArrayList<>());
            user.setAvatar("default.png");
            userDAO.save(user);
        }
    }

//    @Override
//    public void addUser(String username, String password, String email) {
//        if (userDAO.findByUsername(username) == null) {
//            if (userDAO.findByEmail(email) == null) {
//                User user = new User();
//                user.setUsername(username);
//                user.setPassword(password);
//                user.setEmail(email);
//                userDAO.save(user);
//            } else System.out.println("email existed");
//        } else System.out.println("username existed");
//
//
//    }

    @Override
    public void deleteUserByUsername(String username) {
        userDAO.deleteByUsername(username);
    }


    @Override
    public void changeEmail(String username, String newEmail) {
        if (userDAO.findByUsername(username) != null) {
            userDAO.findByUsername(username).setEmail(newEmail);
        } else System.out.println("User not exist");
    }

    @Override
    public List<User> findAll() {
        return userDAO.findAll();
    }


}
