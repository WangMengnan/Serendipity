package com.serendipity.core.service.impl;

import com.serendipity.core.dao.PhotoDAO;
import com.serendipity.core.dao.UserDAO;
import com.serendipity.core.pojo.Photo;
import com.serendipity.core.pojo.User;
import com.serendipity.core.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Niko on 2017/3/9.
 */
@Service
public class PhotoServiceImpl implements PhotoService {
    @Autowired
    private PhotoDAO photoDAO;

    @Autowired
    private UserDAO userDAO;

    @Override
    public void addPhoto(Photo photo) {
        photoDAO.save(photo);
    }

    @Override
    public List<Photo> findPhotoByUserId(String userId) {
        return photoDAO.findPhotoByUserId(userId);
    }

    @Override
    public Photo findPhotoById(String id) {
        return photoDAO.findById(id);
    }

    @Override
    public List<Photo> findAll() {
        return photoDAO.findAll();
    }

    @Override
    public List<Photo> findByTag(String tag) {
        return null;
    }

    @Override
    public boolean updateById(Photo aPhoto) {
        Photo photo = photoDAO.findById(aPhoto.getId());
        System.out.println(photo+"user found");
        photo.setName(aPhoto.getName());
        photo.setCoordinateX(aPhoto.getCoordinateX());
        photo.setCoordinateY(aPhoto.getCoordinateY());
        photo.setText(aPhoto.getText());
        photo.setTag(aPhoto.getTag());
        photo.setIfPrivate(aPhoto.isIfPrivate());

        User user = userDAO.findById(photo.getUserId());
        List<String> photos;
        photos = user.getPhotos();

        photos.add(photo.getId());
        user.setPhotos(photos);


        try {
            userDAO.save(user);
            photoDAO.save(photo);
            System.out.println("save success");
            System.out.println(userDAO.findById(photo.getUserId()).getPhotos());
        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void uploadPhoto(String id, String filePath, String userId, String resourceName) {

        Photo photo = new Photo();
        photo.setId(id);
        photo.setUrl(filePath);
        photo.setDate(new Date());
        photo.setUserId(userId);
        photo.setResourceName(resourceName);
        try {
            photoDAO.save(photo);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public boolean deletePhotoById(String userId, String photoId) {
        boolean flag = false;
        User user = userDAO.findById(userId);
        List<String> photoList = user.getPhotos();
        System.out.println(photoId);
        for (int i=0;i<photoList.size();i++){
            if(photoList.get(i).equals(photoId)){
                System.out.println(photoList.get(i));
                photoList.remove(i);
                flag = true;
                System.out.println("deleted");
            }
        }
        user.setPhotos(photoList);
        userDAO.save(user);
        return flag;
    }


}
