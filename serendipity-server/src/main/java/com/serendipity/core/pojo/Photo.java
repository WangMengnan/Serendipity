package com.serendipity.core.pojo;

import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;


/**
 * Created by Niko on 2016/12/13.
 */
public class Photo {
    @Id
    private String id;
    private String name;
    private Date date;
    private double coordinateX;
    private double coordinateY;
    private String text;
    private String userId;
    private String url;
    private List<String> tag;
    private boolean ifPrivate;
    private String resourceName;

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(double coordinateX) {
        this.coordinateX = coordinateX;
    }

    public double getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(double coordinateY) {
        this.coordinateY = coordinateY;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getTag() {
        return tag;
    }

    public void setTag(List<String> tag) {
        this.tag = tag;
    }

    public boolean isIfPrivate() {
        return ifPrivate;
    }

    public void setIfPrivate(boolean ifPrivate) {
        this.ifPrivate = ifPrivate;
    }


    @Override
    public String toString() {
        return "Photo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", coordinateX=" + coordinateX +
                ", coordinateY=" + coordinateY +
                ", text='" + text + '\'' +
                ", userId='" + userId + '\'' +
                ", url='" + url + '\'' +
                ", tag=" + tag +
                ", ifPrivate=" + ifPrivate +
                '}';
    }
}



