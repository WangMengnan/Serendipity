package com.serendipity.core.pojo;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * Created by Niko on 5/6/2017.
 */
public class Like {
    @Id
    private String id;
    private String userId;
    private String photoId;
    private Date date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
