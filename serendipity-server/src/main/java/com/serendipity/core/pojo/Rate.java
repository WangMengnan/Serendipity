package com.serendipity.core.pojo;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * Created by Niko on 5/7/2017.
 */
public class Rate {
    @Id
    private String id;
    private String photoId;
    private String userId;
    private Date date;
    private int rate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
